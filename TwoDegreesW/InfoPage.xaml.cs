﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
using TwoDegreesShared.ViewModels;

namespace TwoDegreesW
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InfoPage : Page
    {
        private MainPageViewModel MainViewModel { get { return DataContext as MainPageViewModel; } }

        public InfoPage()
        {
            this.InitializeComponent();
            Loaded += (o, e) => Refresh();
        }

        private void refreshButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        	Refresh();
        }
        public async void Refresh()
        {
            /*var statusBar = StatusBar.GetForCurrentView();

            statusBar.ProgressIndicator.Text = "refreshing...";

            await statusBar.ProgressIndicator.ShowAsync();*/
            refreshBar.IsIndeterminate = true;
            await MainViewModel.Login();
            refreshBar.IsIndeterminate = false;
            //await TwoDegreesTileUpdater.UpdateTileAction();

            // await statusBar.ProgressIndicator.HideAsync();
        }

        private void AppBarButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Frame.Navigate(typeof(LoginPage));

        }
    }
}
