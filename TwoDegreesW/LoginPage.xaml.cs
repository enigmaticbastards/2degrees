﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
using TwoDegreesShared.ViewModels;

namespace TwoDegreesW
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        private LoginViewModel DataViewModel { get { return DataContext as LoginViewModel; } }

        public LoginPage()
        {
            this.InitializeComponent();
        }

        private void loginButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        	Login();
            
        }

        private async void Login()
        {
            /*var statusBar = StatusBar.GetForCurrentView();
            statusBar.ProgressIndicator.Text = "logging in...";
            await statusBar.ProgressIndicator.ShowAsync();*/

            var success = await DataViewModel.Login();
            if (success)
                Frame.Navigate(typeof(InfoPage));
            else
            {
                var messageBox = new MessageDialog("Embarrassing! You've gone and got your number or password wrong",
                    "Uh oh!");
                await messageBox.ShowAsync();
            }

            //await statusBar.ProgressIndicator.HideAsync();
        }
    }
}
