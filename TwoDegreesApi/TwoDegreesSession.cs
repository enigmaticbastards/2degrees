﻿using System.Net;

namespace TwoDegreesApi
{
    public class TwoDegreesSession
    {
        internal TwoDegreesSession(CookieContainer cookies, string sessionType)
        {
            CookieJar = cookies;
            SessionType = sessionType;
        }

        internal CookieContainer CookieJar { get; private set; }

        internal string SessionType { get; private set; }
    }
}