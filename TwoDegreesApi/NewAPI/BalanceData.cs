﻿using Newtonsoft.Json;

namespace TwoDegreesApi.NewAPI
{
    /// <summary>
    /// Gives balance data relating to a particular user.
    /// </summary>
    public class BalanceData
    {
        /// <summary>
        /// Todo I mean who knows what this is?
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// General cash on your account
        /// </summary>
        [JsonProperty("General Cash")]
        public string Cash { get; set; }
        /// <summary>
        /// The expiry data of your general cash
        /// </summary>
        public string ExpiryDate { get; set; }
        /// <summary>
        /// The amount that expires on the expiry date
        /// </summary>
        public string ExpiryValue { get; set; }
        /// <summary>
        /// Todo Any special balances you may have, not too sure what this really is
        /// </summary>
        public string SpecialBalance { get; set; }
        /// <summary>
        /// Todo not really sure but my guess is it's the expiry date for any special balances you have
        /// </summary>
        public string SpecialExpiry { get; set; }
        /// <summary>
        /// An introduction message probably not going to use this though
        /// </summary>
        [JsonProperty("IntroductionMsg")]
        public string IntroductionMessage { get; set; }
        /// <summary>
        /// A message about fair use policies for minutes if necessary
        /// </summary>
        [JsonProperty("FairUseMinutesMsg")]
        public string FairUseMinutesMessage { get; set; }
        /// <summary>
        /// A message about fair use policies for texts if necessary
        /// </summary>
        [JsonProperty("FairUseTextMsg")]
        public string FairUseTextMessage { get; set; }
        /// <summary>
        /// A message about fair use policies for data if necessary
        /// </summary>
        [JsonProperty("FairUseDataMsg")]
        public string FairUseDataMessage { get; set; }
        /// <summary>
        /// Mins for calling India?
        /// </summary>
        [JsonProperty("india_mins")]
        public string IndiaMinutes { get; set; }
        /// <summary>
        /// Mins for calling China
        /// </summary>
        [JsonProperty("china_mins")]
        public string ChinaMinutes { get; set; }
        /// <summary>
        /// Todo don't know
        /// </summary>
        [JsonProperty("any_mins")]
        public string AnyMinutes { get; set; }
        /// <summary>
        /// The mins you have on your plan (guessing this is for paymothly people)
        /// </summary>
        [JsonProperty("plan_mins")]
        public string PlanMinutes { get; set; }
        /// <summary>
        /// International Data, guessing it's for use internationally (clever I know)
        /// </summary>
        [JsonProperty("int_data")]
        public string InternationalData { get; set; }
        /// <summary>
        /// National Data, I think although mine was 0
        /// </summary>
        [JsonProperty("nat_data")]
        public string NationalData { get; set; }
        /// <summary>
        /// Mobile Broadband Data, not a clue what this is
        /// </summary>
        [JsonProperty("mbb_data")]
        public string MobileBroadBandData { get; set; }
        /// <summary>
        /// Data from your plan (guessing this is for paymonthly customers)
        /// </summary>
        [JsonProperty("plan_data")]
        public string PlanData { get; set; }
        /// <summary>
        /// Mins from magic min addons
        /// </summary>
        [JsonProperty("magic_mins")]
        public string MagicMins { get; set; }
        /// <summary>
        /// The customers overall minutes
        /// </summary>
        [JsonProperty("mins")]
        public string Minutes { get; set; }
        /// <summary>
        /// The totally number of texts
        /// </summary>
        [JsonProperty("msgs")]
        public string Texts { get; set; }
        /// <summary>
        /// Totally amount of data
        /// </summary>
        [JsonProperty("MB")]
        public string Data { get; set; }
        /// <summary>
        /// Amount of data from a combo
        /// </summary>
        [JsonProperty("comboMB")]
        public string ComboData { get; set; }
        /// <summary>
        /// Number of texts from a combo
        /// </summary>
        [JsonProperty("comboMsg")]
        public string ComboTexts { get; set; }
        /// <summary>
        /// Number of minutes from a combo
        /// </summary>
        [JsonProperty("comboMins")]
        public string ComboMinutes { get; set; }
        /// <summary>
        /// Todo not a clue, probably used to slay dragons
        /// </summary>
        [JsonProperty("calling_offer")]
        public string CallingOffer { get; set; }
        /// <summary>
        /// Message about when your current messages are
        /// expiring and the amount that is expirying
        /// </summary>
        [JsonProperty("expiryCreditMsg")]
        public string CreditExpiryMessage { get; set; }
        /// <summary>
        /// Message about when your current minutes are
        /// expiring and the amount that is expiring
        /// </summary>
        [JsonProperty("expiryMins")]
        public string MinutesExpiryMessage { get; set; }
        /// <summary>
        /// Message about when your current texts are expiring
        /// and the amount that is epiring
        /// </summary>
        [JsonProperty("expiryMsg")]
        public string TextsExpiryMessage { get; set; }
        /// <summary>
        /// Message about when your current data is expiring
        /// and the amount of data that is expiring
        /// </summary>
        [JsonProperty("expiryMB")]
        public string DataExpiryMessage { get; set; }
        /// <summary>
        /// Additional Details about the minutes you have todo find out what this actually is
        /// </summary>
        [JsonProperty("min_details")]
        public string MinutesDetails { get; set; }
        /// <summary>
        /// Additional details about the data you have todo find out what this actually is
        /// </summary>
        [JsonProperty("mb_details")]
        public string DataDetails { get; set; }
    }
}
