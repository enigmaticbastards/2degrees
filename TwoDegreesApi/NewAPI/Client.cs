﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TwoDegreesApi.NewAPI
{
    /// <summary>
    /// Client to be used to login and get balances. May be expanded upon later.
    /// </summary>
    public class Client
    {
        public string LoginURL = "https://mobile.2degreesmobile.co.nz/index.php/login";
        public string BalancesURL = "https://mobile.2degreesmobile.co.nz/index.php/getBalance";
        private readonly HttpClient client = new HttpClient();

        /// <summary>
        /// Gets a login item based on the phone number and password given to it. Currently 
        /// will probably return an empty object if the request fails
        /// </summary>
        /// <param name="phonenumber">Phone number of the person logging in</param>
        /// <param name="password">Password of the person logging in</param>
        /// <returns>A login item based on the json response from logging in</returns>
        public async Task<LoginItem> LoginAsync(string phonenumber, string password)
        {
            var content = new FormUrlEncodedContent(GetLoginRequestBody(phonenumber, password));

            var response = await client.PostAsync(LoginURL, content);

            var json = await response.Content.ReadAsStringAsync();

            var login = JsonConvert.DeserializeObject<LoginItem>(json);

            return login;
        }

        /// <summary>
        /// Creates and returns a Dictionary with the information needed in 
        /// the form for sending away with the login request.
        /// </summary>
        /// <param name="phonenumber">Phone number of person logging in</param>
        /// <param name="password">Password of person logging in</param>
        /// <returns>The dictionary with all the information needed in the form for sending away</returns>
        private static IEnumerable<KeyValuePair<string, string>> GetLoginRequestBody(string phonenumber, string password)
        {
            var formContent = new Dictionary<string, string>
            {
                {"phonenumber", phonenumber},
                {"password", password},
                {"operation", "login"},
                {"version", "1.4"}
            };
            return formContent;
        }

        /// <summary>
        /// Gets the userbalances of a person
        /// </summary>
        /// <param name="login">The login details of the person you want to gets balances</param>
        /// <returns>User balances of the person who the login belongs to</returns>
        public async Task<Balances> GetBalancesAsync(LoginItem login)
        {
            var content = new FormUrlEncodedContent(GetBalancesRequestBody(login.PhoneNumber, login.Token));

            var response = await client.PostAsync(BalancesURL, content);
            var json = await response.Content.ReadAsStringAsync();
            var balances = JsonConvert.DeserializeObject<Balances>(json);

            return balances;
        }

        /// <summary>
        /// Similar to GetLoginRequestBody formats a dictionary of information to submitted
        /// as a form in the get balances request
        /// </summary>
        /// <param name="phonenumber">Phone number of the person who's balances you're getting</param>
        /// <param name="authorisationToken">Authorisation token for getting the balances</param>
        /// <returns>A dictionary with the information needed for the form in the get balances request</returns>
        private static IEnumerable<KeyValuePair<string, string>> GetBalancesRequestBody(string phonenumber, string authorisationToken)
        {
            var formContent = new Dictionary<string, string>
            {
                {"phonenumber", phonenumber},
                {"authtoken", authorisationToken},
                {"operation", "getbalance"}
            };
            return formContent;
        }

        public async Task<WebRecharge> GetWebRechargeToken(LoginItem login)
        {
            var content = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                {"phonenumber", login.PhoneNumber},
                {"authtoken", login.Token},
                {"operation", "webrecharge"},
                {"amount", "20"},
                {"rechargenumber", login.PhoneNumber},
                {"device_id", "1234"},
                {"device_name", "My Phone"}
            }
                );
           

            var response = await client.PostAsync("https://mobile.2degreesmobile.co.nz/index.php/getRechargeWeb2Pay", content);
            var json = await response.Content.ReadAsStringAsync();

            var webRecharge = JsonConvert.DeserializeObject<WebRecharge>(json);

            return webRecharge;
        }
    }
}