﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDegreesApi.NewAPI
{
    /// <summary>
    /// Contains information about user balances
    /// </summary>
    public class Balances
    {
        /// <summary>
        /// Indicates whether or not the the request was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// A description of what went wrong (if something went wrong) on
        /// the 2degrees end. If nothing went wrong this field will be empty
        /// </summary>
        public string Heading { get; set; }

        /// <summary>
        /// The balance data for a user
        /// </summary>
        public BalanceData Data {get; set; }

        /// <summary>
        /// A basic overview of a users minutes
        /// </summary>
        public List<BalanceOverview> AllMinutes { get; set; }

        /// <summary>
        /// A basic overview of a users texts
        /// </summary>
        public List<BalanceOverview> AllTexts { get; set; }

        /// <summary>
        /// A basic overview of a users data
        /// </summary>
        public List<BalanceOverview> AllData { get; set; }
    }
}
