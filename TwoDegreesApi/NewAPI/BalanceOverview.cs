﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TwoDegreesApi.NewAPI
{
    /// <summary>
    /// Info about a specific balances, like those that show
    /// up on the 2degrees website in the tables
    /// </summary>
    public class BalanceOverview
    {
        /// <summary>
        /// The name of the balance. For example 'Carryover Minutes"
        /// </summary>
        [JsonProperty("tagname")]
        public string Name { get; set; }

        /// <summary>
        /// The value of the balance. This could be a number
        /// or something like "Unlimited Texts"
        /// </summary>
        [JsonProperty("tagvalue")]
        public string Value { get; set; }
    }
}
