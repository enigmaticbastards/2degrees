﻿using Newtonsoft.Json;

namespace TwoDegreesApi.NewAPI
{
    /// <summary>
    /// Contains information about a user and whether or not they have
    /// successfully logged in.
    /// </summary>
    public class LoginItem
    {
        /// <summary>
        /// Indicates whether or not the login was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// The phone number of the user
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// The authentication token 
        /// </summary>
        [JsonProperty("AuthToken")]
        public string Token { get; set; }

        /// <summary>
        /// This is only important if you send off a bad request. If you do then this
        /// property will contain information about what happened.
        /// </summary>
        public string Heading { get; set; }

        /// <summary>
        /// The data associated with this user.
        /// </summary>
        public LoginData Data { get; set; }
    }
}