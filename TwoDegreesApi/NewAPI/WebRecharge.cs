﻿using Newtonsoft.Json;

namespace TwoDegreesApi.NewAPI
{
    public class WebRecharge
    {
        public string Success { get; set; }
        public string Data { get; set; }
    }
}