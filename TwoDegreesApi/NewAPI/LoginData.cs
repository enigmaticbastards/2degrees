﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TwoDegreesApi.NewAPI
{
    /// <summary>
    /// Data relating to the person logged in.
    /// </summary>
    public class LoginData
    {
        /// <summary>
        /// The type of contract the customer has
        /// </summary>
        [JsonProperty("contract_type")]
        public string ContractType { get; set; }
        /// <summary>
        /// Not really sure what profile type is yet
        /// </summary>
        [JsonProperty("profile_type")]
        public string ProfileType { get; set; }
        /// <summary>
        /// The first name of the customer
        /// </summary>
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        /// <summary>
        /// Last name of the customer
        /// </summary>
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        /// <summary>
        /// Not really sure what SLC is yet
        /// </summary>
        public string SLC { get; set; }
        /// <summary>
        /// The number to send notifications to?
        /// </summary>
        [JsonProperty("notification_number")]
        public string NotificationNumber { get; set; }
        /// <summary>
        /// Other details about the login including login id and time stamp
        /// </summary>
        [JsonProperty("login_details")]
        public Dictionary<string, string> LoginDetails { get; set; }
    }
}