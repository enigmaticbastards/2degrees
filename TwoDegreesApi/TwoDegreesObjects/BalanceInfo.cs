﻿namespace TwoDegreesApi.TwoDegreesObjects
{
    public class BalanceInfo
    {
        private const string UNLIMITED_MOD = "unlimited";

        private string amount;

        public BalanceInfo()
        {
            Unit = "";
        }

        public string Name { get; set; }

        public string Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                if (amount.ToLower().Contains(UNLIMITED_MOD))
                    amount = UNLIMITED_MOD;
            }
        }

        public string Unit { get; set; }

        public ExpiryInfo ExpiryInfo { get; set; }

        public static BalanceInfo CreateEmpty(string name, string unit = "")
        {
            return new BalanceInfo {Amount = "0", Name = name, Unit = unit};
        }
    }
}