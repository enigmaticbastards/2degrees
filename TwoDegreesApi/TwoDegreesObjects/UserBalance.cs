﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TwoDegreesApi.TwoDegreesObjects
{
    public class UserBalance
    {
        private readonly Dictionary<string, BalanceInfo> balances = new Dictionary<string, BalanceInfo>();
        private DateTime lastRefresh;

        public BalanceInfo DataInfo
        {
            get
            {
                string amount = "";
                float sum = Sum(new[] {"data"});
                if (float.IsPositiveInfinity(sum))
                    amount = "unlimited";
                else amount = sum.ToString();

                return new BalanceInfo {Amount = amount, Name = "data"};
            }
        }

        public BalanceInfo MinutesInfo
        {
            get
            {
                string amount = "";
                float sum = Sum(new[] {"minutes"});
                if (float.IsPositiveInfinity(sum))
                    amount = "unlimited";
                else amount = sum.ToString();

                return new BalanceInfo {Amount = amount, Name = "mins"};
            }
        }

        public BalanceInfo TextsInfo
        {
            get
            {
                string amount = "";
                float sum = Sum(new[] {"texts"});
                if (float.IsPositiveInfinity(sum))
                    amount = "unlimited";
                else amount = sum.ToString();

                return new BalanceInfo {Amount = amount, Name = "texts"};
            }
        }

        public BalanceInfo CreditInfo
        {
            get
            {
                try
                {
                    return
                        Balances.FirstOrDefault(
                            b =>
                                b.Name.ToLower().Contains("bill") || b.Name.ToLower().Contains("credit") ||
                                b.Name.ToLower().Contains("$"));
                }
                catch (Exception e)
                {
                }
                {
                    return new BalanceInfo {Amount = "N/A", Name = "Credit"};
                }
            }
        }

        public List<BalanceInfo> Balances
        {
            get { return balances.Values.ToList(); }
        }

        private float Sum(IEnumerable<string> nameContains)
        {
            IEnumerable<BalanceInfo> valid = Balances.Where(b => nameContains.Any(n => b.Name.ToLower().Contains(n)));

            float sum = 0f;
            foreach (BalanceInfo balanceInfo in valid)
            {
                if (balanceInfo.Amount.Contains("unlimited")) return float.PositiveInfinity;

                float count = 0f;
                float.TryParse(balanceInfo.Amount, out count);
                sum += count;
            }

            return sum;
        }

        public void Set(BalanceInfo balance)
        {
            balances[balance.Name.ToLower()] = balance;
            string newC = balance.Name[0].ToString().ToUpper();
            balance.Name = balance.Name.Remove(0, 1);
            balance.Name = balance.Name.Insert(0, newC);
        }

        public DateTime LastRefresh
        {
            get { return lastRefresh; }
            set { lastRefresh = value; }
        }
    }
}