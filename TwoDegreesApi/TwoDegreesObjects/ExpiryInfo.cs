﻿namespace TwoDegreesApi.TwoDegreesObjects
{
    public class ExpiryInfo
    {
        public string Amount { get; set; }
        public string ExpiringOn { get; set; }

        public override string ToString()
        {
            return string.Format("({0} expiring on {1})", Amount, ExpiringOn);
        }
    }
}