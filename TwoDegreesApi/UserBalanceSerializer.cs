﻿using System;
using System.IO;
using System.Linq;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegreesApi
{
    public static class UserBalanceSerializer
    {
        public static void Save(Stream to, UserBalance balance)
        {
            var writer = new BinaryWriter(to);

            writer.Write(balance.Balances.Count());
            foreach (BalanceInfo info in balance.Balances)
            {
                SaveInfo(writer, info);
            }
            writer.Write(balance.LastRefresh.ToString());
            writer.BaseStream.Flush();
        }

        private static void SaveInfo(BinaryWriter writer, BalanceInfo info)
        {
            writer.Write(info.Name);
            writer.Write(info.Amount);
            writer.Write(info.Unit);
            writer.Write(info.ExpiryInfo != null);
            if (info.ExpiryInfo == null) return;

            writer.Write(info.ExpiryInfo.Amount);
            writer.Write(info.ExpiryInfo.ExpiringOn);
        }

        private static BalanceInfo LoadInfo(BinaryReader reader)
        {
            string name = reader.ReadString();
            string amount = reader.ReadString();
            string unit = reader.ReadString();

            ExpiryInfo info = null;

            if (reader.ReadBoolean())
            {
                string exAmout = reader.ReadString();
                string exInfo = reader.ReadString();
                info = new ExpiryInfo {Amount = exAmout, ExpiringOn = exInfo};
            }

            return new BalanceInfo {Amount = amount, Name = name, ExpiryInfo = info, Unit = unit};
        }

        public static UserBalance Load(Stream from)
        {
            var reader = new BinaryReader(from);
            int count = reader.ReadInt32();

            var balance = new UserBalance();
            for (int i = 0; i < count; ++i)
            {
                BalanceInfo info = LoadInfo(reader);
                balance.Set(info);
            }
            balance.LastRefresh = DateTime.Parse(reader.ReadString());
            return balance;
        }
    }
}