﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace TwoDegreesApi
{
    public class TwoDegreesLoginHelper
    {
        private const string LOGIN_PAGE_URL =
            "https://secure.2degreesmobile.co.nz/web/ip/login?p_auth=bfwc3yfB&p_p_id=customerportalloginportlet_WAR_customerportalloginportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-2&p_p_col_count=2&_customerportalloginportlet_WAR_customerportalloginportlet_struts_action=%2Fext%2Flogin%2Fhome";

        private readonly HttpClient client;
        private readonly HttpClientHandler handler;

        internal string Html;

        public TwoDegreesLoginHelper()
        {
            handler = new HttpClientHandler {UseCookies = true, CookieContainer = new CookieContainer()};
            client = new HttpClient(handler);
        }

        public CookieContainer CookieJar
        {
            get { return handler.CookieContainer; }
        }

        public bool LoggedIn { get; private set; }

        public async Task<TwoDegreesSession> Login(string username, string password)
        {
            try
            {
                var content = new FormUrlEncodedContent(GetLoginRequestBody(username, password));
                //var baseUri = new Uri("https://secure.2degreesmobile.co.nz/web/ip/login");

                var uri = new Uri(LOGIN_PAGE_URL);
                HttpResponseMessage response = await client.PostAsync(uri, content);

                Html = await response.Content.ReadAsStringAsync();

                //string requestURI = response.RequestMessage.RequestUri.ToString();
                string type = null;

                var parser = new HtmlDocument();
                parser.LoadHtml(Html);

                HtmlNode doc = parser.DocumentNode;

                var accountLink = doc.FindFirstWithClass("btn sm no-margin-bottom").Attributes["href"].Value;

                if (accountLink.Contains("home"))
                    type = "prepay";
                else if (accountLink.Contains("postpaid"))
                    type = "paymonthly";

                return await Task.Run(() => Html.Contains("Logout") ? new TwoDegreesSession(CookieJar, type) : null);
            }
            catch
            {
                return null;
            }
        }

        private static IEnumerable<KeyValuePair<string, string>> GetLoginRequestBody(string username, string password)
        {
            var formContent = new Dictionary<string, string>
            {
                {"userid", username},
                {"password", password},
                {"hdnAction", "login_userlogin"},
                {"hdnAuthenticationType", "M"},
            };
            return formContent;
        }
    }
}