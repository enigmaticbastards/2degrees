﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using TwoDegreesApi.Exceptions;
using TwoDegreesApi.Parsers;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegreesApi
{
    public class TwoDegreesClient
    {
        private const string PREPAY_BALANCE_PAGE_URL = "https://secure.2degreesmobile.co.nz/group/ip/home";
        private const string PAY_MONTHLY_BALANCE_PAGE_URL = "https://secure.2degreesmobile.co.nz/group/ip/postpaid";

        private readonly HttpClient client;
        private readonly TwoDegreesSession session;
        
        /// <summary>
        /// Sets up a HttpClient that is used for retrieving the balances html.
        /// Makes the client imitate mozilla firefox on a computer.
        /// </summary>
        /// <param name="session">The TwoDegreesSession which contains the cookies that allow us to retrieve the html for parsing balances</param>
        public TwoDegreesClient(TwoDegreesSession session)
        {
            this.session = session;
            if (session == null) return;

            var handler = new HttpClientHandler
            {
                UseCookies = true,
                CookieContainer = session.CookieJar,
                AllowAutoRedirect = false
            };

            client = new HttpClient(handler);
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
        }

        private Exception GetBalanceException = new Exception("No web exceptions");
        private string userType = "No user type";


        /// <summary>
        /// Retrieves the user balances
        /// </summary>
        /// <param name="retrys">The number of times it will try to download the html</param>
        /// <returns></returns>
        public async Task<UserBalance> GetBalanceAsync(int retrys)
        {
            if (retrys > 0)
            {
                string html = null;
                try
                {
                    switch (session.SessionType)
                    {
                        case "prepay":
                            userType = "prepay";
                            html = await client.GetStringAsync(new Uri(PREPAY_BALANCE_PAGE_URL));
                            break;
                        case "paymonthly":
                            userType = "paymonthly";
                            html = await client.GetStringAsync(new Uri(PAY_MONTHLY_BALANCE_PAGE_URL));
                            break;
                    }
                }
                catch (Exception e)
                {
                    GetBalanceException = e;
                }

                if (html == null) return await GetBalanceAsync(retrys - 1);
                return await Task.Run(() => GetBalance(html));
            }
            throw new NoInternetException("Error getting balances. Have gremlins eaten your internet?", GetBalanceException, userType);
        }

        /// <summary>
        /// Parses the html page containing the user balances.
        /// </summary>
        /// <param name="html">The html to be parsed</param>
        /// <returns></returns>
        public UserBalance GetBalance(string html)
        {
            var parser = new HtmlDocument();
            parser.LoadHtml(html);

            HtmlNode doc = parser.DocumentNode;

            HtmlNode table = doc.FindFirstWithClass("tableBillSummary tableAccountSummary");
            //throw new TwoDegreesParsingException(html, session.CookieJar.GetCookies(new Uri(BALANCE_PAGE_URL)), "Couldn't find table 'tableBillSummary tableAccountSummary'");
            if (table == null)
                throw new TwoDegreesParsingException(html, session.CookieJar.GetCookies(new Uri(PREPAY_BALANCE_PAGE_URL)),
                    "Couldn't find table 'tableBillSummary tableAccountSummary'");

            IEnumerable<HtmlNode> rows = table.Descendants("tr");
            var result = new UserBalance();

            //throw new Exception("This exception is stupid and has no purpose");

            foreach (HtmlNode row in rows)
            {
                HtmlNode nameCell = row.FindFirstWithClass("tableBilldetail");
                HtmlNode infoCell = row.FindFirstWithClass("tableBillamount");

                if (infoCell == null) continue;

                try
                {
                    string name = WebUtility.HtmlDecode(row.Descendants("strong").First().InnerText);
                    string amount = Helpers.CleanString(ParseHtmlString(infoCell.InnerText), false, false);
                    ExpiryInfo expiryInfo = ExpiryParser.Parse(nameCell);
                    string unit = "";
                    if (name == "credit")
                        unit = "$";
                    if (name == "data")
                        unit = "MB";
                    result.Set(new BalanceInfo {Amount = amount, ExpiryInfo = expiryInfo, Name = name, Unit = unit});
                }
                catch (Exception e)
                {
                    throw new TwoDegreesParsingException(html, session.CookieJar.GetCookies(new Uri(PREPAY_BALANCE_PAGE_URL)),
                        string.Format("Unable to parse '{0}' cell", nameCell.InnerText), e);
                }
            }

            result.LastRefresh = DateTime.Now;

            return result;
        }

        private string ParseHtmlString(string parse)
        {
            return WebUtility.HtmlDecode(parse);
        }
    }
}