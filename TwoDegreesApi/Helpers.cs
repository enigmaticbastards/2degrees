﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace TwoDegreesApi
{
    internal static class Helpers
    {
        internal static HtmlNode NextNode(this HtmlNode node, string name = null)
        {
            HtmlNode next = node.NextSibling;

            if (name == null) return next;

            while (next != null && next.Name != name)
                next = next.NextSibling;

            return next;
        }

        internal static IEnumerable<HtmlNode> FindWithClass(this HtmlNode node, string className)
        {
            return from child in node.Descendants()
                where child.Attributes.Contains("class") && child.Attributes["class"].Value == className
                select child;
        }

        internal static HtmlNode FindFirstWithClass(this HtmlNode node, string className)
        {
            return node.FindWithClass(className).FirstOrDefault();
        }

        internal static string CleanString(string input, bool numbersOnly = true, bool noDecimalPoints = true)
        {
            string result = input;
            result = result.Replace((char) 160, ' ');
            result = result.Trim().Trim('(', ')');

            int firstSpace = result.IndexOf(" ");
            if (firstSpace != -1)
                result = result.Substring(0, firstSpace);

            if (numbersOnly && char.IsNumber(result[0]))
            {
                int last = 0;
                while (last < result.Length && "1234567890.".Contains(result[last] + ""))
                    last++;
                result = result.Substring(0, last);
            }

            if (noDecimalPoints && !input.Contains("$"))
            {
                int dp = result.IndexOf(".");
                if (dp != -1)
                {
                    result = result.Substring(0, dp);
                }
            }

            return result;
        }

        internal static int IndexOfAny(this string indexIn, string[] of)
        {
            foreach (string s in of)
            {
                int index = indexIn.IndexOf(s);
                if (index != -1) return index;
            }

            return -1;
        }

        internal static string FirstOccurence(this string inString, string[] of)
        {
            foreach (string s in of)
            {
                if (inString.Contains(s))
                    return s;
            }

            return null;
        }
    }
}