﻿using System;
using System.Net;

namespace TwoDegreesApi.Exceptions
{
    public class TwoDegreesParsingException : Exception
    {
        public TwoDegreesParsingException(string html, CookieCollection cookies, string message = "",
            Exception innerException = null)
            : base(message, innerException)
        {
            Html = html;
            Cookies = cookies;
        }

        public string Html { get; private set; }
        public CookieCollection Cookies { get; private set; }
    }
}