﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDegreesApi.Exceptions
{
    public class NoInternetException : Exception
    {
        public string UserType;

        public NoInternetException(string message, Exception innerException = null, string userType = null)
        {
            UserType = userType;
        }


    }
}
