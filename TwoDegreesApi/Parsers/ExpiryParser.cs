﻿using System.Linq;
using HtmlAgilityPack;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegreesApi.Parsers
{
    internal class ExpiryParser
    {
        private static readonly string[] expiryTags = {"expiring on ", "expire on "};

        public static ExpiryInfo Parse(HtmlNode nameCell)
        {
            HtmlNode expiryCell = nameCell.Descendants("em").FirstOrDefault();
            if (expiryCell == null) return null;

            string text = expiryCell.InnerText.Trim().Trim('(', ')');

            int startIndex = text.IndexOfAny(expiryTags);
            if (startIndex == -1 || startIndex == 0) return null;

            string amount = text.Substring(0, startIndex - 1);
            amount = amount.Replace("Unlimited Text*", "All");
            if (char.IsNumber(amount[0]))
                amount = Helpers.CleanString(amount);

            string date = text.Substring(startIndex + text.FirstOccurence(expiryTags).Length);

            return new ExpiryInfo {Amount = amount, ExpiringOn = date};
        }
    }
}