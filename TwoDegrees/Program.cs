﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using TwoDegreesApi;
using TwoDegreesApi.Exceptions;
using TwoDegreesApi.NewAPI;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegrees
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //MagicCookieTests();
            //RunTests();
            //DownloadTheStats();
            NewLogin();
            Console.ReadKey();
        }

        private static async void NewLogin()
        {
            var client = new Client();
            var login = await client.LoginAsync("0221074528", "Sunshine45");
            //var balances = await client.GetBalancesAsync(login);
            var stuff = await client.GetWebRechargeToken(login);
        }

        private static async void DownloadTheStats()
        {
            var helper = new TwoDegreesLoginHelper();
            await helper.Login("0221074528", "Sunshine45");
            var cookies = helper.CookieJar;

            var client = new HttpClient(new HttpClientHandler {UseCookies = true, CookieContainer = cookies});
            client.DefaultRequestHeaders.Add("User-Agent", " Mozilla/5.0 (Windows NT 10.0; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0");
            client.DefaultRequestHeaders.Add("Referer", "https://secure.2degreesmobile.co.nz/group/ip/itemizedcalls");
            var url = "https://secure.2degreesmobile.co.nz/yourUsage/jsps/yourusage/YourUsageCDRS.jsp?startDate=0&endDate=0&usageType=0&pageNavigation=0";

            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["startDate"] = "01/02/2015";
            query["endDate"] = "28/02/2015";
            query["usageType"] = "4";
            query["pageNavigation"] = "0";
            uriBuilder.Query = query.ToString();
            url = uriBuilder.ToString();

            var response = await client.GetStringAsync(new Uri(url));
            //var stuff = await response.

        }

        private static void MagicCookieTests()
        {
            Console.WriteLine("Starting Magic Cookie Tests!");

            var uriInvolvingCookies = new Uri("https://www.google.co.nz/");
            var handler = new HttpClientHandler();
            handler.UseCookies = true;

            Console.WriteLine("Getting Stuffs");
            var client = new HttpClient(handler);
            var task = client.GetAsync(uriInvolvingCookies);
            task.Wait();
            Console.WriteLine("Got stuffs");
            Console.WriteLine("\nReading cookies");

            var cookies = handler.CookieContainer.GetCookies(uriInvolvingCookies);

            foreach (var cookie in cookies)
                Console.WriteLine("Cookie: {0}", cookie);

            Console.WriteLine("\nDone! Press any key to continue with whatever else Jay's made go on here...");
            Console.ReadKey();
        }
        public static void RunTests()
        {
            var cookieJar = new CookieContainer();
            var request = (HttpWebRequest) WebRequest.Create("http://www.google.com");
            request.CookieContainer = cookieJar;

            var response = (HttpWebResponse) request.GetResponse();
            foreach (object cookie in response.Cookies)
            {
                Console.WriteLine(cookie.ToString());
            }
            var files = Directory.GetFiles(@"C:\Users\James\Source\Repos\2degrees\TwoDegrees\Test\");
            foreach (var file in files)
            {
                if (file.EndsWith(".html"))
                    RunTest(file);
            }
        }

        private static void RunTest(string path)
        {
            var client = new TwoDegreesClient(null);

            try
            {
                UserBalance balances = client.GetBalance((new StreamReader(File.OpenRead(path))).ReadToEnd());
                //await client.GetBalanceAsync();

                foreach (var balance in balances.Balances)
                {
                    Console.Write("{0}: {1}", balance.Name, balance.Amount);
                    if (balance.ExpiryInfo != null)
                        Console.Write(" ({0} expiring on {1})\n", balance.ExpiryInfo.Amount,
                            balance.ExpiryInfo.ExpiringOn);
                    else Console.WriteLine();
                }

                Console.WriteLine("Success!\n");
            }
            catch (TwoDegreesParsingException e)
            {
                Console.WriteLine("Caught Exception!");
                TextWriter writer = new StreamWriter(File.Create("Bad Html.html"));
                writer.Write(e.Html);
                writer.Close();

                Exception innerException = e.InnerException;
                writer = new StreamWriter(File.Create("Log.txt"));

                while (innerException != null)
                {
                    writer.WriteLine(e.Message);
                    writer.WriteLine();

                    innerException = innerException.InnerException;
                }

                writer.Close();
            }
        }
    }
}