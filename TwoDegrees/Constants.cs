﻿namespace TwoDegrees
{
    internal class Constants
    {
        public const string LOGIN_PAGE_URL =
            "https://secure.2degreesmobile.co.nz/web/ip/login?p_auth=bfwc3yfB&p_p_id=customerportalloginportlet_WAR_customerportalloginportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-2&p_p_col_count=2&_customerportalloginportlet_WAR_customerportalloginportlet_struts_action=%2Fext%2Flogin%2Fhome";

        public const string LOGIN_CONTENT_FORMAT_STRING =
            "userid=0220972663&password=Oxymoron72&hdnAction=login_userlogin&hdnlocale=&hdnAuthenticationType=M&externalURLRedirect=";
    }
}