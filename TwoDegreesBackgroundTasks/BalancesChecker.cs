﻿using System;
using Windows.UI.Notifications;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegreesBackgroundTasks
{
    public static class BalancesChecker
    {
        /// <summary>
        /// Checks the current balances to see if any of the balances are low and sends notifications if they are.
        /// Also checks to see if your balances have gone up and therefore deserve the notification sent setting to be reset.
        /// </summary>
        /// <param name="balance">The balances that need to be checked</param>
        internal static void CheckLowBalances(UserBalance balance)
        {
            float creditOrSpent, data, mins, texts;

            foreach (var balanceInfo in balance.Balances)
            {
                if (balanceInfo.Name.ToLower().Contains("credit") &&
                    float.TryParse(balanceInfo.Amount, out creditOrSpent))
                {
                    if (creditOrSpent < 5 && !Settings.CreditNotificationSent)
                    {
                        CreateNotification(creditOrSpent, "credit");
                        Settings.CreditNotificationSent = true;
                    }
                    else if (creditOrSpent >= 5 && Settings.CreditNotificationSent)
                        Settings.CreditNotificationSent = false;
                }
                else if (balanceInfo.Name.ToLower().Contains("minute") &&
                         float.TryParse(balanceInfo.Amount, out mins))
                {
                    if (mins < 10 && !Settings.MinNotificationSent)
                    {
                        CreateNotification(mins, "minutes");
                        Settings.MinNotificationSent = true;
                    }
                    else if (mins >= 10 && Settings.MinNotificationSent)
                        Settings.MinNotificationSent = false;
                }
                else if (balanceInfo.Name.ToLower().Contains("data")  &&
                         float.TryParse(balanceInfo.Amount, out data) )
                {
                    if (data < 20 && !Settings.DataNotificationSent)
                    {
                        CreateNotification(data, "data");
                        Settings.DataNotificationSent = true;
                    }
                    else if (data >= 20 && Settings.DataNotificationSent)
                        Settings.DataNotificationSent = false;
                }
                else if (balanceInfo.Name.ToLower().Contains("text") &&
                         float.TryParse(balanceInfo.Amount, out texts))
                {
                    if (texts < 20 && !Settings.TextNotificationSent)
                    {
                        CreateNotification(texts, "texts");
                        Settings.TextNotificationSent = true;
                    }
                    else if (texts >= 20 && Settings.TextNotificationSent)
                        Settings.TextNotificationSent = false;
                }
            }
        }

        /// <summary>
        /// Creates a notification for when a balance is low
        /// </summary>
        /// <param name="warningAmount">The amount that caused the warning</param>
        /// <param name="type">The type of warning it is</param>
        private static void CreateNotification(float warningAmount, string type)
        {
            const ToastTemplateType toastTemplate = ToastTemplateType.ToastText04;
            var toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);

            var toastTextElements = toastXml.GetElementsByTagName("text");

            //Adding the text for the notification
            toastTextElements[0].AppendChild(toastXml.CreateTextNode("Warning!"));
            //Determining the text by the type
            var notificationText = "";
            switch (type)
            {
                case "credit":
                    notificationText += String.Format("You only have ${0} left of credit", warningAmount);
                    break;
                case "texts":
                    notificationText += String.Format("You only have {0} texts left", warningAmount);
                    break;
                case "data":
                    notificationText += String.Format("You only have {0}MB of data left", warningAmount);
                    break;
                case "minutes":
                    notificationText += String.Format("You only have {0} mins left", warningAmount);
                    break;
            }
            toastTextElements[1].AppendChild(toastXml.CreateTextNode(notificationText));
            
            
            var toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
