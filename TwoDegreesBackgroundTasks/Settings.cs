﻿using System;
using System.Collections.Generic;
using Windows.Storage;

namespace TwoDegreesBackgroundTasks
{
    internal static class Settings
    {
        #region Setting Names

        private const string OPENS = "Opens";
        private const string PASSWORD = "Password";
        private const string USERNAME = "Username";
        private const string NOTIFICATIONS = "Notifications";

        private const string DATA = "Data";
        private const string MONEY = "Money";
        private const string MINUTES = "Minutes";
        private const string TEXTS = "Texts";

        private const string LOGIN_CONTAINER = "Login";
        private const string INFO_CONTAINER = "Info";

        private const string DATANOTIFICATION = "DataNotification";
        private const string MINNOTIFICATION = "MinNotification";
        private const string TEXTNOTIFICATION = "TextNotification";
        private const string CREDITNOTIFICATION = "CreditNotification";


        #endregion

        #region Fields

        private static readonly ApplicationDataContainer roamingSettings = ApplicationData.Current.RoamingSettings;
        private static readonly ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        #endregion

        #region Helpers

        private static ApplicationDataContainer LoginContainer
        {
            get { return LoginExists ? roamingSettings.Containers[LOGIN_CONTAINER] : null; }
        }

        private static ApplicationDataContainer InfoContainer
        {
            get { return InfoExists ? localSettings.Containers[INFO_CONTAINER] : null; }
        }

        public static bool LoginExists
        {
            get { return roamingSettings.Containers.ContainsKey(LOGIN_CONTAINER); }
        }

        private static bool InfoExists
        {
            get { return localSettings.Containers.ContainsKey(INFO_CONTAINER); }
        }

        private static void SaveSetting(string name, object value, ApplicationDataContainer container)
        {
            if (!container.Values.ContainsKey(name))
                container.Values.Add(name, "");
            container.Values[name] = value;
        }

        #endregion

        #region Settings

        public static int Opens
        {
            get { return localSettings.Values.ContainsKey(OPENS) ? (int) localSettings.Values[OPENS] : 0; }
            set { SaveSetting(OPENS, value, localSettings); }
        }

        public static bool NotificationsOn
        {
            get
            {
                return Convert.ToBoolean(localSettings.Values[NOTIFICATIONS]);
            }
            set { SaveSetting(NOTIFICATIONS, value, localSettings); }
        }

        public static bool DataNotificationSent
        {
            get { return Convert.ToBoolean(localSettings.Values[DATANOTIFICATION]); }
            set { SaveSetting(DATANOTIFICATION, value, localSettings);}
        }

        public static bool MinNotificationSent
        {
            get { return Convert.ToBoolean(localSettings.Values[MINNOTIFICATION]); }
            set { SaveSetting(MINNOTIFICATION, value, localSettings); }
        }

        public static bool CreditNotificationSent
        {
            get { return Convert.ToBoolean(localSettings.Values[CREDITNOTIFICATION]); }
            set { SaveSetting(CREDITNOTIFICATION, value, localSettings); }
        }

        public static bool TextNotificationSent
        {
            get { return Convert.ToBoolean(localSettings.Values[TEXTNOTIFICATION]); }
            set { SaveSetting(TEXTNOTIFICATION, value, localSettings); }
        }

        internal static string Username
        {
            get { return !LoginExists ? "" : LoginContainer.Values[USERNAME] as string; }
            set
            {
                if (!LoginExists)
                    roamingSettings.CreateContainer(LOGIN_CONTAINER, ApplicationDataCreateDisposition.Always)
                        ;
                SaveSetting(USERNAME, value, LoginContainer);
            }
        }

        internal static string Password
        {
            get { return !LoginExists ? "" : LoginContainer.Values[PASSWORD] as string; }
            set
            {
                if (!LoginExists)
                    roamingSettings.CreateContainer(LOGIN_CONTAINER, ApplicationDataCreateDisposition.Always)
                        ;
                SaveSetting(PASSWORD, value, LoginContainer);
            }
        }

        #endregion
    }
}