﻿using System;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using TwoDegreesApi;
using TwoDegreesApi.TwoDegreesObjects;
using System.Threading.Tasks;

namespace TwoDegreesBackgroundTasks
{
    public sealed class UpdateTileBackgroundTask : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            await Update();

            deferral.Complete();
        }

        public static IAsyncAction UpdateAction()
        {
            return Update().AsAsyncAction();
        }

        internal async static Task Update()
        {
            string username = Settings.Username;
            string password = Settings.Password;

            var loginHelper = new TwoDegreesLoginHelper();
            TwoDegreesSession session = await loginHelper.Login(username, password);
            if (session == null) return;

            var client = new TwoDegreesClient(session);
            UserBalance balances = null;
            try
            {
                balances = await client.GetBalanceAsync(3);
            }
            catch (Exception e)
            {
                return;
            }
            if (Settings.NotificationsOn) BalancesChecker.CheckLowBalances(balances);
            await TwoDegreesTileUpdater.UpdateTile(balances);
        }
    }
}