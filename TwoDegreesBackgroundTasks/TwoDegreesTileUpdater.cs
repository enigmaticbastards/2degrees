﻿using System;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.UI.Notifications;
using TwoDegreesApi;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegreesBackgroundTasks
{
    public static class TwoDegreesTileUpdater
    {
        internal static async Task UpdateTile(UserBalance balances)
        {
            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();

            if (balances.Balances.Count == 0) return;

            string title = string.Format("{1} ${0}", balances.Balances[0].Amount,
                balances.Balances[0].Name.Contains("spend") ? "Spent" : "Credit");
            string content = string.Format("{0}MB\n{1} minutes\n{2} texts", balances.DataInfo.Amount,
                balances.MinutesInfo.Amount, balances.TextsInfo.Amount);

            XmlDocument tile = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150PeekImageAndText02);

            XmlNodeList images = tile.GetElementsByTagName("image");
            ((XmlElement) images[0]).SetAttribute("src", "ms-appx:///assets/Logo.scale-140.png");

            XmlNodeList textBlocks = tile.GetElementsByTagName("text");
            textBlocks[0].InnerText = title;
            textBlocks[1].InnerText = content;
            updater.Update(new TileNotification(tile));

            tile = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150PeekImage01);
            images = tile.GetElementsByTagName("image");
            ((XmlElement) images[0]).SetAttribute("src", "ms-appx:///assets/WideLogo.scale-240.png");

            textBlocks = tile.GetElementsByTagName("text");
            textBlocks[0].InnerText = title;
            textBlocks[1].InnerText = content;
            updater.Update(new TileNotification(tile));
        }
    }
}