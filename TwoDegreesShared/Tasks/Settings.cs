﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;
using TwoDegreesApi;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegreesShared.Tasks
{
    public static class Settings
    {
        #region Setting Names

        private const string LAST_OPENED = "last_opened";
        private const string OPENS = "Opens";
        private const string PASSWORD = "Password";
        private const string USERNAME = "Username";
        private const string NOTIFICATIONS = "Notifications";

        private const string LOGIN_CONTAINER = "Login";
        private const string INFO_CONTAINER = "Info";

        #endregion

        #region Fields

        private static readonly ApplicationDataContainer roamingSettings = ApplicationData.Current.RoamingSettings;
        private static readonly ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        #endregion

        #region Helpers

        private static ApplicationDataContainer LoginContainer
        {
            get { return LoginExists ? roamingSettings.Containers[LOGIN_CONTAINER] : null; }
        }

        private static ApplicationDataContainer InfoContainer
        {
            get { return InfoExists ? localSettings.Containers[INFO_CONTAINER] : null; }
        }

        public static bool LoginExists
        {
            get { return roamingSettings.Containers.ContainsKey(LOGIN_CONTAINER); }
        }

        private static bool InfoExists
        {
            get { return localSettings.Containers.ContainsKey(INFO_CONTAINER); }
        }

        private static void SaveSetting(string name, object value, ApplicationDataContainer container)
        {
            if (!container.Values.ContainsKey(name))
                container.Values.Add(name, "");
            container.Values[name] = value;
        }

        #endregion

        #region Settings

        private static UserBalance balance;

        public static string CurrentVersion
        {
            get { return string.Format("{0}.{1}", Package.Current.Id.Version.Major, Package.Current.Id.Version.Minor); }
        }

        public static string LastVersionOpened
        {
            get
            {
                return localSettings.Values.ContainsKey(LAST_OPENED) ? localSettings.Values[LAST_OPENED] as string : "";
            }
            set { SaveSetting(LAST_OPENED, value, localSettings); }
        }

        public static bool NotificationsOn
        {
            get
            {
                bool notifications = Convert.ToBoolean(localSettings.Values[NOTIFICATIONS]);
                return notifications;
            }
            set { SaveSetting(NOTIFICATIONS, value, localSettings);}
        }

        public static int Opens
        {
            get { return localSettings.Values.ContainsKey(OPENS) ? (int) localSettings.Values[OPENS] : 0; }
            set { SaveSetting(OPENS, value, localSettings); }
        }

        public static string Username
        {
            get { return !LoginExists ? "" : LoginContainer.Values[USERNAME] as string; }
            set
            {
                if (!LoginExists)
                    roamingSettings.CreateContainer(LOGIN_CONTAINER, ApplicationDataCreateDisposition.Always)
                        ;
                SaveSetting(USERNAME, value, LoginContainer);
            }
        }

        public static string Password
        {
            get { return !LoginExists ? "" : LoginContainer.Values[PASSWORD] as string; }
            set
            {
                if (!LoginExists)
                    roamingSettings.CreateContainer(LOGIN_CONTAINER, ApplicationDataCreateDisposition.Always)
                        ;
                SaveSetting(PASSWORD, value, LoginContainer);
            }
        }

        public static UserBalance Balance
        {
            get { return balance; }
            set
            {
                balance = value;
                SaveUserBalance(balance);
            }
        }

        public static async Task<string> GetAbout()
        {
            StorageFolder storage = Package.Current.InstalledLocation;
            StorageFile aboutFile = await storage.GetFileAsync("Assets\\About.txt");

            TextReader reader = new StreamReader(await aboutFile.OpenStreamForReadAsync());
            string aboutString = await reader.ReadToEndAsync();

            PackageVersion v = Package.Current.Id.Version;
            string version = string.Format("{0}.{1}", v.Major, v.Minor);
            aboutString = string.Format(aboutString, version, "enigmatic_bastards@outlook.com");
            return aboutString;
        }

        public static async Task<string> GetChangelog()
        {
            StorageFolder storage = Package.Current.InstalledLocation;
            StorageFile changelogFile = await storage.GetFileAsync("Assets\\Changelog.txt");

            TextReader reader = new StreamReader(await changelogFile.OpenStreamForReadAsync());
            string changelog = await reader.ReadToEndAsync();

            return changelog;
        }

        private static async void SaveUserBalance(UserBalance newBalance)
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("balance.dat",
                    CreationCollisionOption.ReplaceExisting);
                Stream stream = await file.OpenStreamForWriteAsync();
                UserBalanceSerializer.Save(stream, newBalance);
                stream.Dispose();
            }
            catch (Exception e)
            {
            }
        }

        public static async Task LoadUserBalance()
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("balance.dat");
                Stream stream = await file.OpenStreamForReadAsync();

                balance = UserBalanceSerializer.Load(stream);
                stream.Dispose();
            }
            catch
            {
                balance = new UserBalance();
            }
        }

        #endregion
    }
}