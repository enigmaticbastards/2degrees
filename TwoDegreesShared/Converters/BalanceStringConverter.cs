﻿using System;
using Windows.UI.Xaml.Data;
using TwoDegreesApi;
using TwoDegreesApi.TwoDegreesObjects;

namespace TwoDegreesShared.Converters
{
    public class BalanceStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var balance = value as BalanceInfo;

            if (balance.Unit == "$")
                return string.Format("{0}{1}", balance.Unit, balance.Amount);
            return string.Format("{1}{0}", balance.Unit, balance.Amount);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}