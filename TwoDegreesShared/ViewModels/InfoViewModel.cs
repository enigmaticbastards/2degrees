﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml.Controls;
using TwoDegreesApi;
using TwoDegreesApi.TwoDegreesObjects;
using TwoDegreesShared.Commands;
using TwoDegreesShared.Tasks;

namespace TwoDegreesShared.ViewModels
{
    public class InfoViewModel : INotifyPropertyChanged
    {
        private UserBalance balance = new UserBalance();

        public UserBalance UserBalance
        {
            get { return balance; }
            set
            {
                if (balance == value) return;

                balance = value;

                Balances.Clear();
                foreach (BalanceInfo b in UserBalance.Balances)
                    Balances.Add(b);


                OnPropertyChanged();

                SaveInfo();
            }
        }

        public ObservableCollection<BalanceInfo> Balances { get; private set; }
        public SimpleCommand RefreshCommand { get; private set; }
        public SimpleCommand SettingsCommand { get; private set; }
        public SimpleCommand AboutCommand { get; private set; }

        public InfoViewModel()
        {
            Balances = new ObservableCollection<BalanceInfo>();

            RefreshCommand = new SimpleCommand();
            RefreshCommand.Executed += (sender, o) =>;

            SettingsCommand = new SimpleCommand();
            SettingsCommand.Executed += (sender, o) => NavigateToPage<Settings>();

            AboutCommand = new SimpleCommand();
            AboutCommand.Executed += (sender, o) =>;
            LoadInfo();
        }

        private void NavigateToPage<T>()
            where T : Page
        {
            
        }

        private void LoadInfo()
        {
            UserBalance = Settings.Balance;
        }

        private void SaveInfo()
        {
            Settings.Balance = balance;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string propertyName=null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}