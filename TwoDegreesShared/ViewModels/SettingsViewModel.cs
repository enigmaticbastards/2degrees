﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TwoDegreesShared.Tasks;

namespace TwoDegreesShared.ViewModels
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        private bool notifications;

        /// <summary>
        /// Indicates whether the user should recieve notifications
        /// </summary>
        public bool Notifications
        {
            get { return notifications; }
            set
            {
                if (notifications == value) return;

                notifications = value;
                OnPropertyChanged();

                SaveNotificationsSetting();
            }
        }

        public SettingsViewModel()
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            notifications = Settings.NotificationsOn;
        }

        public void SaveNotificationsSetting()
        {
            Settings.NotificationsOn = notifications;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string propertyName=null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
