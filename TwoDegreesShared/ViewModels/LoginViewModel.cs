﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using TwoDegreesApi;
using TwoDegreesShared.Tasks;

namespace TwoDegreesShared.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private readonly TwoDegreesLoginHelper loginHelper;
        private string password;
        private string username;

        public TwoDegreesSession Session { get; private set; }

        public string Username
        {
            get { return username; }
            set
            {
                if (username == value) return;

                username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                if (password == value) return;

                password = value;
                OnPropertyChanged();
            }
        }

        public LoginViewModel()
        {
            loginHelper = new TwoDegreesLoginHelper();
            Password = Settings.Password;
            Username = Settings.Username;
        }

        public async Task<bool> Login()
        {
            try
            {
                Session = await loginHelper.Login(Username, Password);
                if (Session == null && !TestHelper.IsTest(this)) return false;
            }
            catch
            {
                return false;
            }
            Settings.Username = Username;
            Settings.Password = Password;

            return true;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}