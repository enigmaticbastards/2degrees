﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Email;
using Windows.ApplicationModel.Store;
using Windows.System;
using TwoDegreesShared.Annotations;
using TwoDegreesShared.Commands;
using TwoDegreesShared.Tasks;

namespace TwoDegreesShared.ViewModels
{
    public class AboutViewModel : INotifyPropertyChanged
    {
        private string aboutText;

        /// <summary>
        /// The AboutText for the app
        /// </summary>
        public string AboutText
        {
            get { return aboutText; }
            private set
            {
                if (value == aboutText) return;
                aboutText = value;
                OnPropertyChanged();
            }
        }

        public SimpleCommand ReviewCommand { get; private set; }
        public SimpleCommand SendFeedbackCommand { get; private set; }

        public AboutViewModel()
        {
            ReviewCommand = new SimpleCommand();
            ReviewCommand.Executed += (o, e) => Review();

            SendFeedbackCommand = new SimpleCommand();
            SendFeedbackCommand.Executed += (o, e) => SendFeedback();

            Initialize();
        }

        private async void Initialize()
        {
            AboutText = await Settings.GetAbout();
        }

        private async void Review()
        {
            await Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=" + CurrentApp.AppId));
        }

        private async void SendFeedback()
        {
            var email = new EmailMessage();
            email.To.Add(new EmailRecipient("enigmatic_bastards@outlook.com"));
            email.Subject = "2Degrees Feedback";
            email.Body = "SOMETHING BROKE!";

            await EmailManager.ShowComposeNewEmailAsync(email);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
