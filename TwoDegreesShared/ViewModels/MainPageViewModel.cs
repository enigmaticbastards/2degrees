﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.UI.Popups;
using TwoDegreesApi;

namespace TwoDegreesShared.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public InfoViewModel Info { get; private set; }
        public LoginViewModel LoginView { get; private set; }

        public bool LoggedIn { get; private set; }

        public MainPageViewModel()
        {
            Info = new InfoViewModel();
            LoginView = new LoginViewModel();
        }

        public async Task Refresh()
        {
            if (TestHelper.IsTest(LoginView))
            {
                Info.UserBalance = TestHelper.BuildTestBalance();
                LoggedIn = true;
                return;
            }

            if (!LoggedIn && !await LoginView.Login())
            {
                var messageBox =
                    new MessageDialog(
                        "Embarrassing! Looks like we've failed to automatically login for you (or you're not connected to the internet). Please check your internet and then try logging in again.",
                        "Uh oh!");
                await messageBox.ShowAsync();
                LoggedIn = false;
                return;
            }

            LoggedIn = true;
            var client = new TwoDegreesClient(LoginView.Session);
            var balances = await client.GetBalanceAsync(3);
            Info.UserBalance = balances;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}