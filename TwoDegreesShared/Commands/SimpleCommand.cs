﻿using System;
using System.Windows.Input;

namespace TwoDegreesShared.Commands
{
    public class SimpleCommand : ICommand
    {
        private bool executable;
        public event EventHandler<object> Executed;

        public bool Executable
        {
            get { return executable; }
            set
            {
                executable = value;
                if (CanExecuteChanged != null) CanExecuteChanged(this, EventArgs.Empty);
            }
        }

        public SimpleCommand()
        {
            executable = true;
        }
 
        public bool CanExecute(object parameter)
        {
            return Executable;
        }

        public void Execute(object parameter)
        {
            if (Executed != null)
                Executed(this, parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
