﻿using System;
using Windows.UI.Xaml.Data;

namespace TwoDegreesWP.Converters
{
    public class LastRefreshStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var time = value is DateTime ? (DateTime) value : new DateTime();

            if (time == new DateTime())
            {
                return "No last refresh data currently";
            }

            var dateString = "Last Updated: ";
            dateString += time.ToString("d/M") + " at " + time.ToString("t");
            return dateString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
