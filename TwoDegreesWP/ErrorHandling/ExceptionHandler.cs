﻿using System;
using Windows.UI.Popups;

namespace TwoDegreesWP.ErrorHandling
{
    public static class ExceptionHandler
    {
        /// <summary>
        ///     For logging any exceptions that may occur.
        /// </summary>
        /// <param name="e"></param>
        public static void LogException(Exception e)
        {
            //TODO if we actually want to log exceptions

            throw new NotImplementedException("This method isn't implemented yet");
        }

        /// <summary>
        ///     Handles any exceptions passed to it by giving the option of sending us the error email or closing the application.
        /// </summary>
        /// <param name="e"></param>
        public static MessageDialog HandleException()
        {
            //Debug.WriteLine(e.Message);
            var okayButton = new UICommand("Sure");
            var noButton = new UICommand("No Thanks");
            var errorBox =
                new MessageDialog(
                    "There was an error in the application. Would you like to send us information on it so that we can hopefully fix it? If this continues try restarting the application",
                    "Uh oh!");

            errorBox.Commands.Add(okayButton);
            errorBox.Commands.Add(noButton);

            return errorBox;
        }

    }
}