﻿using TwoDegreesApi.TwoDegreesObjects;
using TwoDegreesWP.ViewModels;

namespace TwoDegreesWP
{
    public class TestHelper
    {
        public const string TEST_NUMBER = "1234567890";
        public const string TEST_PASSWORD = "foobar";

        public static UserBalance BuildTestBalance()
        {
            var balance = new UserBalance();
            balance.Set(new BalanceInfo
            {
                Amount = "Several",
                ExpiryInfo = new ExpiryInfo {Amount = "A few", ExpiringOn = "Never"},
                Name = "Texts"
            });
            return balance;
        }

        public static bool IsTest(LoginViewModel login)
        {
            return login.Username == TEST_NUMBER && login.Password == TEST_PASSWORD;
        }
    }
}