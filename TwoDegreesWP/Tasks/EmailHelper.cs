﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Email;
using Windows.Storage;
using Windows.Storage.Streams;
using TwoDegreesApi.Exceptions;

namespace TwoDegreesWP.Tasks
{
    public class EmailHelper
    {
        /// <summary>
        ///     Sends a error email to us based on the exception thrown up from the bowls of this silly application.
        /// </summary>
        /// <param name="exception">The exception being sent to us.</param>
        /// <returns></returns>
        public static async Task SendErrorEmail(Exception exception)
        {
            var email = new EmailMessage();
            email.To.Add(new EmailRecipient("enigmatic_bastards@outlook.com"));
            var version = Package.Current.Id.Version;
            email.Subject = "2Degrees Error Version: " + version.Major + "." + version.Minor + "." + version.Revision;
            string body =
                "Stuff broken everywhere!!!\n\nThe part below this is to help us, please don't delete it.\n\n" +
                "Error\n\n" + exception.StackTrace + "\n\n" + exception.Message;

            if (exception is TwoDegreesParsingException)
            {
                StorageFile htmlFile = await MakeFile((exception as TwoDegreesParsingException).Html, "html");
                string cookies = (exception as TwoDegreesParsingException).Cookies.Cast<object>().Aggregate("Cooooooookkkkkkkkieeeessssss!!!!!!!\n\n", (current, cookie) => current + (cookie + "\n"));
                StorageFile cookiesFile =
                    await MakeFile(cookies, "cookies");
                if (htmlFile != null && cookiesFile != null)
                {
                    RandomAccessStreamReference stream = RandomAccessStreamReference.CreateFromFile(htmlFile);
                    var htmlAttachment = new EmailAttachment(htmlFile.Name, stream);
                    stream = RandomAccessStreamReference.CreateFromFile(cookiesFile);
                    var cookiesAttachment = new EmailAttachment(cookiesFile.Name, stream);
                    email.Attachments.Add(htmlAttachment);
                    email.Attachments.Add(cookiesAttachment);
                }
            }
            else if (exception is NoInternetException)
            {
                body += exception.InnerException == null ? "\n\nThere was a no internet Exception as well" : "\n\nMORE EXCEPTIONS\n\n" + exception.InnerException.StackTrace + "\n\n" + exception.InnerException.Message;
                body += ((NoInternetException) exception).UserType == null
                    ? ""
                    : "\nUser Type: " + ((NoInternetException) exception).UserType;
            }
            email.Body = body;
            await EmailManager.ShowComposeNewEmailAsync(email);
        }

        /// <summary>
        ///     Creates a file from a given string (doesn't check if it is html) in the temporary storage.
        /// </summary>
        /// <param name="info">The info to be made into a file.</param>
        /// <param name="type">The type of file to make it.</param>
        /// <returns></returns>
        private static async Task<StorageFile> MakeFile(string info, string type)
        {
            switch (type)
            {
                case "html":
                    {
                        StorageFile file =
                            await
                                ApplicationData.Current.TemporaryFolder.CreateFileAsync("errorHtml.html",
                                    CreationCollisionOption.ReplaceExisting);
                        await FileIO.WriteTextAsync(file, info);
                        return file;
                    }
                case "cookies":
                    {
                        StorageFile file =
                            await
                                ApplicationData.Current.TemporaryFolder.CreateFileAsync("errorCookies.txt",
                                    CreationCollisionOption.ReplaceExisting);
                        await FileIO.WriteTextAsync(file, info);
                        return file;
                    }
            }
            return null;
        }
    }

}
