﻿using System;
using Windows.ApplicationModel.Background;

namespace TwoDegreesWP.Tasks
{
    public static class BackgroundTaskHelper
    {
        public static async void RegisterBackgroundTask(string entryPoint)
        {
            string name = entryPoint.Substring(entryPoint.IndexOf(".") + 1);

            BackgroundAccessStatus backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();
            if (backgroundAccessStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
                backgroundAccessStatus == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity)
            {
                foreach (var task in BackgroundTaskRegistration.AllTasks)
                {
                    if (task.Value.Name == name)
                    {
                        task.Value.Unregister(true);
                    }
                }

                var taskBuilder = new BackgroundTaskBuilder();
                taskBuilder.Name = name;
                taskBuilder.TaskEntryPoint = entryPoint;
                taskBuilder.SetTrigger(new TimeTrigger(15, false));
                BackgroundTaskRegistration registration = taskBuilder.Register();
            }
        }
    }
}