﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;
using Windows.UI.ViewManagement;
using TwoDegreesWP.Properties;

namespace TwoDegreesWP.ViewModels
{
    public enum ProgressMessageType
    {
        LoggingIn,
        Refreshing,
    }
    public class ProgressIndicatorViewModel : INotifyPropertyChanged
    {
        private static readonly Random random = new Random();

        private readonly StatusBarProgressIndicator progressIndicator = StatusBar.GetForCurrentView().ProgressIndicator;
        
        private List<string> messages = new List<string>(); 

        private bool isVisible;
        private string text;
        private float progress;
        private bool usesRandomMessage;

        public bool IsVisible
        {
            get { return isVisible; }
            set
            {
                if (value.Equals(isVisible)) return;
                isVisible = value;
                OnPropertyChanged();

                if (isVisible)
                    progressIndicator.ShowAsync();
                else progressIndicator.HideAsync();
            }
        }

        public string Text
        {
            get { return text; }
            set
            {
                if (value == text) return;
                text = value;
                OnPropertyChanged();

                progressIndicator.Text = value;
            }
        }

        public float Progress
        {
            get { return progress; }
            set
            {
                if (value.Equals(progress)) return;
                progress = value;
                OnPropertyChanged();

                progressIndicator.ProgressValue = progress;
            }
        }

        public bool UsesRandomMessage
        {
            get { return usesRandomMessage; }
            set
            {
                if (value.Equals(usesRandomMessage)) return;
                usesRandomMessage = value;
                OnPropertyChanged();
            }
        }

        public List<string> Messages { get { return messages; } set { messages = value; } } 

        public ProgressIndicatorViewModel(List<string> messages=null)
        {
            this.messages = messages;
            UsesRandomMessage = true;
        }

        public void Show(string message = null)
        {
            if (UsesRandomMessage && messages != null && messages.Count > 0)
                progressIndicator.Text = messages[random.Next(messages.Count)];
            else if (message != null)
                progressIndicator.Text = message;
            else progressIndicator.Text = "";

            IsVisible = true;
        }

        public void Hide()
        {
            IsVisible = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public static async Task<List<string>> LoadMessages(ProgressMessageType message)
        {
            var fileName = "";

            switch (message)
            {
                case ProgressMessageType.LoggingIn:
                    fileName = "LoginMessages";
                    break;
                case ProgressMessageType.Refreshing:
                    fileName = "RefreshMessages";
                    break;
            }

            fileName = string.Format(@"{0}\{1}.txt", @"Assets\ProgressMessages", fileName);

            var file = await Package.Current.InstalledLocation.GetFileAsync(fileName);

            var messages = await FileIO.ReadLinesAsync(file);
            return messages.ToList();
        }
    }
}
