﻿using System;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using TwoDegreesApi;
using TwoDegreesWP.Commands;
using TwoDegreesWP.Pages;
using TwoDegreesWP.Tasks;

namespace TwoDegreesWP.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private int failedLoginAttempts;

        private readonly TwoDegreesLoginHelper loginHelper;
        private string password;
        private string username;

        public ProgressIndicatorViewModel ProgressIndicator { get; private set; }

        public TwoDegreesSession Session { get; private set; }

        public SimpleCommand LoginCommand { get; private set; }

        public string Username
        {
            get { return username; }
            set
            {
                if (username == value) return;

                username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                if (password == value) return;

                password = value;
                OnPropertyChanged();
            }
        }

        public LoginViewModel()
        {
            loginHelper = new TwoDegreesLoginHelper();

            ProgressIndicator = new ProgressIndicatorViewModel();
            
            LoginCommand = new SimpleCommand();
            LoginCommand.Executed += (sender, o) => ManageLogin();

            Initialize();

            Password = Settings.Password;
            Username = Settings.Username;
        }

        private async void Initialize()
        {
            ProgressIndicator.Messages = await ProgressIndicatorViewModel.LoadMessages(ProgressMessageType.LoggingIn);
        }

        /// <summary>
        /// Manages the login process, dealing with exceptions and problems that may occur.
        /// </summary>
        public async void ManageLogin()
        {
            ProgressIndicator.Show();

            var internetConnection = NetworkInterface.GetIsNetworkAvailable();
            if (internetConnection)
            {
                bool success = await Login();
                if (success)
                {
                    var frame = Window.Current.Content as Frame;
                    if (frame != null)
                        frame.Navigate(typeof(InfoPage));
                }
                else
                {
                    failedLoginAttempts++;
                    MessageDialog messageBox;
                    if (failedLoginAttempts > 2)
                    {
                        messageBox = new MessageDialog("You have failed to login 3 times or more. You will now have to reset your password at 2degrees.", "Uh oh");
                    }
                    else
                    {
                        messageBox = new MessageDialog(
                                        "Embarrassing! You've gone and got your number or password wrong.",
                                        "Uh oh!");
                    }
                    await messageBox.ShowAsync();
                }
            }
            else
            {
                var noConnectionMessage =
                    new MessageDialog(
                        "Unfortunately we don't appear to be able to connect to the internet. Please check your internet connection.",
                        "Uh oh!");
                await noConnectionMessage.ShowAsync();
            }

            ProgressIndicator.Hide();
        }

        /// <summary>
        /// Logs in to 2degrees getting the cookies necessary for retrieving balances.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Login()
        {
            try
            {
                Session = await loginHelper.Login(Username, Password);
                if (Session == null && !TestHelper.IsTest(this)) return false;
            }
            catch
            {
                return false;
            }
            Settings.Username = Username;
            Settings.Password = Password;

            return true;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}