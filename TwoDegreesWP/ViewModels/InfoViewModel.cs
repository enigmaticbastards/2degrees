﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using TwoDegreesApi.TwoDegreesObjects;
using TwoDegreesWP.Commands;
using TwoDegreesWP.Pages;
using TwoDegreesWP.Tasks;

namespace TwoDegreesWP.ViewModels
{
    public class InfoViewModel : INotifyPropertyChanged
    {
        private UserBalance balance = new UserBalance();

        /// <summary>
        /// The current userbalance.
        /// </summary>
        public UserBalance UserBalance
        {
            get { return balance; }
            set
            {
                if (balance == value) return;

                balance = value;

                Balances.Clear();
                foreach (BalanceInfo b in UserBalance.Balances)
                    Balances.Add(b);


                OnPropertyChanged();

                SaveInfo();
            }
        }

        public ObservableCollection<BalanceInfo> Balances { get; private set; }
        public SimpleCommand SettingsCommand { get; private set; }
        public SimpleCommand AboutCommand { get; private set; }

        public InfoViewModel()
        {
            Balances = new ObservableCollection<BalanceInfo>();

            SettingsCommand = new SimpleCommand();
            SettingsCommand.Executed += (sender, o) => NavigateToPage<SettingsPage>();

            AboutCommand = new SimpleCommand();
            AboutCommand.Executed += (sender, o) => NavigateToPage<AboutPage>();
            LoadInfo();
        }

        /// <summary>
        /// Navigates to page of type T
        /// </summary>
        /// <typeparam name="T">The page type to navigate to.</typeparam>
        private void NavigateToPage<T>()
            where T : Page
        {
            var frame = Window.Current.Content as Frame;
            if (frame != null) frame.Navigate(typeof (T));
        }

        /// <summary>
        /// Loads the latest balance info.
        /// </summary>
        private void LoadInfo()
        {
            UserBalance = Settings.Balance;
        }

        /// <summary>
        /// Saves the current balance info.
        /// </summary>
        private void SaveInfo()
        {
            Settings.Balance = balance;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string propertyName=null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}