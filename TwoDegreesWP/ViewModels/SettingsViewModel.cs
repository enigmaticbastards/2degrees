﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using TwoDegreesApi.TwoDegreesObjects;
using TwoDegreesWP.Commands;
using TwoDegreesWP.Pages;
using TwoDegreesWP.Tasks;

namespace TwoDegreesWP.ViewModels
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        private bool notifications;

        /// <summary>
        /// Indicates whether the user should recieve notifications
        /// </summary>
        public bool Notifications
        {
            get { return notifications; }
            set
            {
                if (notifications == value) return;

                notifications = value;
                OnPropertyChanged();

                SaveNotificationsSetting();
            }
        }

        public SimpleCommand LogoutCommand { get; private set; }

        public SettingsViewModel()
        {
            LogoutCommand = new SimpleCommand();
            LogoutCommand.Executed += (o, e) => Logout();

            LoadSettings();
        }

        private void Logout()
        {
            Settings.Username = "";
            Settings.Password = "";
            Settings.Balance = new UserBalance();
            Settings.NotificationsOn = false;
            (Window.Current.Content as Frame).Navigate(typeof(LoginPage));
        }

        private void LoadSettings()
        {
            notifications = Settings.NotificationsOn;
        }

        public void SaveNotificationsSetting()
        {
            Settings.NotificationsOn = notifications;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string propertyName=null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
