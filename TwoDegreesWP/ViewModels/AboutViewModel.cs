﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.ApplicationModel.Email;
using Windows.ApplicationModel.Store;
using Windows.System;
using TwoDegreesWP.Commands;
using TwoDegreesWP.Properties;
using TwoDegreesWP.Tasks;

namespace TwoDegreesWP.ViewModels
{
    public class AboutViewModel : INotifyPropertyChanged
    {
        private string aboutText;

        /// <summary>
        /// The AboutText for the app
        /// </summary>
        public string AboutText
        {
            get { return aboutText; }
            private set
            {
                if (value == aboutText) return;
                aboutText = value;
                OnPropertyChanged();
            }
        }

        public SimpleCommand ReviewCommand { get; private set; }
        public SimpleCommand SendFeedbackCommand { get; private set; }

        public AboutViewModel()
        {
            ReviewCommand = new SimpleCommand();
            ReviewCommand.Executed += (o, e) => Review();

            SendFeedbackCommand = new SimpleCommand();
            SendFeedbackCommand.Executed += (o, e) => SendFeedback();

            Initialize();
        }

        private async void Initialize()
        {
            AboutText = await Settings.GetAbout();
        }

        /// <summary>
        /// Launches the Windows Store app to review the app.
        /// </summary>
        private async void Review()
        {
            await Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=" + CurrentApp.AppId));
        }

        /// <summary>
        /// Opens an email message to send us feedback.
        /// </summary>
        private async void SendFeedback()
        {
            var email = new EmailMessage();
            email.To.Add(new EmailRecipient("enigmatic_bastards@outlook.com"));
            email.Subject = "2Degrees Feedback";
            email.Body = "SOMETHING BROKE!";

            await EmailManager.ShowComposeNewEmailAsync(email);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// In Jay's words, "It's black magic!"
        /// </summary>
        /// <param name="propertyName"></param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
