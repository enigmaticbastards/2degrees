﻿using System;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using TwoDegreesApi;
using TwoDegreesApi.Exceptions;
using TwoDegreesBackgroundTasks;
using TwoDegreesWP.Commands;
using TwoDegreesWP.Tasks;

namespace TwoDegreesWP.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private bool refreshing;

        public InfoViewModel Info { get; private set; }
        public LoginViewModel LoginView { get; private set; }

        public bool LoggedIn { get; private set; }
        public SimpleCommand RefreshCommand { get; private set; }
        public ProgressIndicatorViewModel ProgressIndicator { get; private set; }

        public MainPageViewModel()
        {
            Info = new InfoViewModel();
            LoginView = new LoginViewModel();

            ProgressIndicator = new ProgressIndicatorViewModel();

            RefreshCommand = new SimpleCommand();
            RefreshCommand.Executed += (sender, o) => ManageRefresh();

            Initialize();
        }

        private async void Initialize()
        {
            ProgressIndicator.Messages = await ProgressIndicatorViewModel.LoadMessages(ProgressMessageType.Refreshing);
            ManageRefresh();
        }

        public async void ManageRefresh()
        {
            //TODO This function can probably be merged with 'Refresh.' Also, it should be cleaned up a bit
            if (refreshing) return;

            refreshing = true;

            ProgressIndicator.Show();

            Exception exception = null;
            var internetConnection = NetworkInterface.GetIsNetworkAvailable();
            if (internetConnection)
            {
                try
                {
                    await Refresh();
                    await UpdateTileBackgroundTask.UpdateAction();
                }
                catch (Exception e)
                {
                    exception = e;
                }

                if (exception != null && !(exception is NoInternetException))
                {
                    ProgressIndicator.Hide();
                    var errorBox = new MessageDialog("We failed to magic your 2degrees stuff from the internet :'(" +
                                                     "\n\nWould you mind sending us an email? (this might contain some of your 2degrees balance info)" +
                                                     "\n\nAlso try closing and reopening, sometimes that replenishes our magic, no promises though",
                        "Uh oh!");

                    var okayButton = new UICommand("Sure");
                    errorBox.Commands.Add(okayButton);
                    errorBox.Commands.Add(new UICommand("Nope :'("));

                    IUICommand result = await errorBox.ShowAsync();
                    if (result == okayButton)
                        await EmailHelper.SendErrorEmail(exception);
                }
                else if (exception != null) //So if it is a NoInternetException
                {
                    var result = await ShowNoInternetDialog();
                    if (result.Label == "Send Email") await EmailHelper.SendErrorEmail(exception);
                }
            }
            else
            {
                var result = await ShowNoInternetDialog();
                if (result.Label == "Send Email") await EmailHelper.SendErrorEmail(new NoInternetException("Phone doesn't think it has internet"));
            }

            ProgressIndicator.Hide();
            refreshing = false;
        }

        private static IAsyncOperation<IUICommand> ShowNoInternetDialog()
        {
            var noInternet =
    new MessageDialog(
        "It looks like you might not be connected to the internet, please connect and try again. If this continues to happen please send us an email so we can look into this problem. Your previous balances will still display below",
        "Uh oh");
            noInternet.Commands.Add(new UICommand("OK"));
            noInternet.Commands.Add(new UICommand("Send Email"));
            return noInternet.ShowAsync();
        }

        public async Task Refresh()
        {
            if (TestHelper.IsTest(LoginView))
            {
                Info.UserBalance = TestHelper.BuildTestBalance();
                LoggedIn = true;
                return;
            }

            if (!LoggedIn && !await LoginView.Login())
            {
                var messageBox =
                    new MessageDialog(
                        "Embarrassing! Looks like we've failed to automatically login for you (or you're not connected to the internet). Please check your internet and then try logging in again.",
                        "Uh oh!");
                await messageBox.ShowAsync();
                LoggedIn = false;
                return;
            }

            LoggedIn = true;
            var client = new TwoDegreesClient(LoginView.Session);
            var balances = await client.GetBalanceAsync(3);
            Info.UserBalance = balances;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}